SELECT 
m.created_at,
o.external_ref, 
m.target, 
ac.email,
o.name,
SPLIT_PART(SPLIT_PART(metadata -> 'ticket' ->> 'Ticket','Medidor Nro. ',2),'"',1) AS medidor_mide, 
metadata ->> 'last_token' as comprobante_mide,
m.metadata -> 'operation' -> 'operation_response' ->> 'company_client_id'  as celular_recargado  
FROM ledger.transaction t
LEFT JOIN ledger.origin o USING (origin_id)
LEFT JOIN ledger.movement m ON m.id = t.movement_id
LEFT JOIN app.account ac ON o.external_ref = ac.account_hash
WHERE t.transaction_type_id IN (20,21,29) 
	AND m.target IN ('PERSONAL', 'CLARO', 'MOVISTAR', 'tuenti', 'MIDE') 
ORDER BY m.created_at DESC