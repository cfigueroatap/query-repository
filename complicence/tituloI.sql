-- El titulo I trae todas las transacciones de QR del mes designado.

WITH x1 AS (SELECT CASE
                       WHEN ac1.document IS NOT NULL
                           THEN ac1.document
                       ELSE ap1.cuit
                       END        AS cuit,
                   abs(m1.amount) as amount,
                   CASE
                       WHEN transaction_type_id = 7
                           THEN qr.importe_comision
                       WHEN o1.name IN ('Edenor S.A', 'Litoral Gas') AND transaction_type_id IN (32, 33)
                           THEN (m1.amount * 0.008) * 1.21
                       WHEN transaction_type_id = 5
                           THEN m1.amount * (0.006 * 1.21) / (1 - 0.006 * 1.21)
                       ELSE 0
                       END        AS importe_comision,
                   o1.external_ref
            FROM tap_l1_pg_ledger_public.movement m1
                     LEFT JOIN tap_l1_pg_ledger_public.transaction t1 ON t1.movement_id = m1.id
                     LEFT JOIN tap_l1_pg_ledger_public.origin o1 ON m1.origin_id::integer = o1.origin_id
                     LEFT JOIN tap_l1_pg_app_public.account a1 ON o1.external_ref = a1.account_hash
                     LEFT JOIN tap_l1_pg_app_public.company ac1 ON a1.account_id = ac1.account_id
                     LEFT JOIN tap_l1_pg_app_public.person ap1 ON a1.account_id = ap1.account_id
                     LEFT JOIN tap_l1_pg_fiat_public.confirm_qr_debit qr
                               ON json_extract_path_text(m1.metadata, 'payload', 'data', 'id') = qr.id_debin
                     LEFT JOIN tap_l1_pg_fiat_public.origin fo1 ON o1.external_ref = fo1.external_ref
            WHERE o1.external_ref <> 'tap-wc'
              AND date(m1.created_at) >= '2021-01-01'
              and date(m1.created_at) <= '2021-01-31'
              --AND ac1.document = '30657866330'
              AND ((transaction_type_id = 7 AND m1.details ILIKE '%no identificado%')
                OR (transaction_type_id IN (32, 33) AND m1.amount > 0)
                OR (transaction_type_id = 5 AND json_extract_path_text(m1.metadata, 'is_qr') = 'true' AND
                    m1.amount > 0))
),

x2 AS (SELECT sum(amount) as monto_total, sum(importe_comision) as comision_total, external_ref
FROM x1
GROUP BY external_ref)




SELECT
       CASE
           WHEN ac1.document IS NOT NULL
               THEN 80
           ELSE 86
           END AS identificador,
       CASE
            WHEN ac1.document IS NOT NULL
               THEN ac1.document
            ELSE ap1.cuit
            END AS cuit,
        CASE
            WHEN o1.name IN ('Edenor S.A', 'Litoral Gas') THEN 7
                ELSE 8
            END AS codigo_rubro,
        0 as signo_monto_total,
        CASE
            WHEN transaction_type_id = 7
                THEN round(qr.importe_comision)
            WHEN o1.name IN ('Edenor S.A', 'Litoral Gas') AND transaction_type_id IN (32,33)
                THEN round((m1.amount*0.008)*1.21)
            WHEN transaction_type_id = 5
                THEN round(m1.amount*(0.006 * 1.21) / (1 - 0.006 * 1.21))
            ELSE 0
            END AS importe_comision,
       02 as metodologia_acreditaciones,
       fo1.internal_ref as cvu_vendedor,
       0 as signo_monto,
       round(abs(m1.amount)) as amount,
       round(x2.monto_total) as monto_total,
       round(x2.comision_total) as comision_total,
       m1.created_at
FROM tap_l1_pg_ledger_public.movement m1
        LEFT JOIN tap_l1_pg_ledger_public.transaction t1 ON t1.movement_id = m1.id
        LEFT JOIN tap_l1_pg_ledger_public.origin o1 ON m1.origin_id::integer = o1.origin_id
        LEFT JOIN tap_l1_pg_app_public.account a1 ON o1.external_ref = a1.account_hash
        LEFT JOIN tap_l1_pg_app_public.company ac1 ON a1.account_id = ac1.account_id
        LEFT JOIN tap_l1_pg_app_public.person ap1 ON a1.account_id = ap1.account_id
        LEFT JOIN tap_l1_pg_fiat_public.confirm_qr_debit qr
                   ON json_extract_path_text(m1.metadata, 'payload', 'data', 'id') = qr.id_debin
        LEFT JOIN tap_l1_pg_fiat_public.origin fo1 ON o1.external_ref = fo1.external_ref
    LEFT JOIN x2 ON o1.external_ref = x2.external_ref
WHERE o1.external_ref <> 'tap-wc'
       AND date(m1.created_at) >= '2021-01-01'
              and date(m1.created_at) <= '2021-01-31'
  AND ((transaction_type_id = 7 AND m1.details ILIKE '%no identificado%')
        OR (transaction_type_id IN (32,33) AND m1.amount >0)
             OR (transaction_type_id = 5 AND json_extract_path_text(m1.metadata, 'is_qr') = 'true' AND m1.amount > 0))
;