WITH
cohort_meters as 
(
	select 
		meter_number,
		to_char(min(created_at),'yyyymm') as created_month,
		min(created_at) as created_date
	from tap_l1_pg_lightning_public.transactions
	group by 1
)
,transactions as 
(
select 
		meter_number,
		to_char((created_at),'yyyymm') as transaction_month,
		min(created_at) as transaction_date
	from tap_l1_pg_lightning_public.transactions
	group by 1,2
)
,cohort_size as (
  select 
	created_month, 
	count(1) as qty_meters
  from cohort_meters
  group by 1
  order by 1
)
, meter_retention as (
  select
    M.created_month
    ,(DATE_PART('year', t.transaction_date) - DATE_PART('year', m.created_date)) * 12 +
              (DATE_PART('month', t.transaction_date) - DATE_PART('month', m.created_date)) as month_number,
    count(1) as qty_meters
  from transactions T
  left join cohort_meters M ON T.meter_number = M.meter_number
  group by 1, 2
)

select
  R.created_month as Cohort,
  S.qty_meters as M0,
  R.qty_meters as M_N,
  R.month_number as Month_Number,
  R.qty_meters::float * 100 / S.qty_meters as ret_ratio
from meter_retention R
left join cohort_size S ON R.created_month = S.created_month
where R.created_month IS NOT NULL
order by 1, 4