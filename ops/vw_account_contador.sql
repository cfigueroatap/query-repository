SELECT 
ac.account_id,
ac.email,
ac.phone,
ac.account_hash as external_ref,
ac.created_at as fecha_creacion,
ac.lifetested,
pe.name as name_person,
pe.last_name as last_name_person, 
pe.document_id as dni_person, 
pe.cuit as cuit_person,
co.name as name_company,
co.document as document_company,
al.street,
al.street_number,
al.postal_address,
pr.name as province
FROM app.account ac
LEFT JOIN app.person pe USING (account_id)
LEFT JOIN app.company co USING (account_id)
LEFT JOIN app.account_location al USING (account_id)
LEFT JOIN app.province pr ON al.province_id = pr.province_id