/* Análisis de baja de cuentas
- Se crea tabla nueva para consumir desde Tableau o QS
1. Información de transacciones/movimientos; Tabla física nueva (DROP & CREATE)
2. Información a nivel usuario --> Tabla temporal; volumen aceptable
3. Se crea bi.cx_account_kpis con cálculos a nivel usuario
5. Pendiente: agregar información de cupones
*/ ---------------------------------------------------------------------------------------------------------------------------------------
--Changelog
--Ejecutado y creado 20210928
--Ejecutado 20211109
---------------------------------------------------------------------------------------------------------------------------------------
--Se eliminan tablas físicas para volver a crearlas

DROP TABLE IF EXISTS bi.cx_user_movements;

CREATE TABLE bi.cx_user_movements AS
	SELECT 
		mov.id as movement_id
		,mov.private
		,mov.amount
		,mov.origin_id
		,tran.transaction_type_id as tt
		,mov.origin_type
	FROM ledger.movement as mov
	LEFT JOIN ledger.transaction as tran
		ON (mov.id = tran.movement_id)
	WHERE date(mov.created_at) > '2021-01-01'
		AND not private;

CREATE INDEX index_origin_id
    ON bi.cx_user_movements USING btree
    (origin_id ASC NULLS LAST);

DROP TABLE IF EXISTS bi.cx_account_kpis;

--Tabla de transacciones

CREATE TABLE bi.cx_account_kpis AS
  (
  	WITH 
		blacklist AS
		(SELECT bl.external_ref
			,min(bl.inserted_at) as inserted_at
		FROM auth.blacklist as bl
		GROUP BY bl.external_ref),

		accounts AS
		 (SELECT ori.external_ref AS external_ref,
			 ori.origin_id :: text AS origin_id,
			 date(ori.created_at) AS date_created,
			 date(ori.deleted_at) AS date_deleted,
			 CASE WHEN ori.deleted_at IS NOT NULL THEN 1 ELSE 0 END AS flg_deleted,
			 CASE WHEN ori.deleted_at IS NOT NULL THEN ori.deleted_at - ori.created_at ELSE NULL END AS account_duration,
			 CASE WHEN acc.lifetested = TRUE THEN 1 ELSE 0 END AS flg_lifetest,
			 acc.account_type_id,
			 acc.device_id,
			 CASE WHEN bl.external_ref IS NOT NULL THEN 1 ELSE 0 END AS flg_blacklist,
			 date(bl.inserted_at) AS date_blacklisted
		  FROM ledger.origin ori
		  LEFT JOIN app.account acc ON ori.external_ref = acc.account_hash
		  LEFT JOIN blacklist bl ON ori.external_ref = bl.external_ref) 
   
   SELECT acc.external_ref,
		acc.origin_id,
		acc.date_created,
		acc.date_deleted,
		acc.flg_deleted,
		acc.account_duration,
		acc.flg_lifetest,
		acc.account_type_id,
		acc.device_id,
		acc.flg_blacklist,
		acc.date_blacklisted,
		COUNT(DISTINCT mov.movement_id) AS count_transactions,
		COUNT(DISTINCT CASE WHEN tt IN (1, 22) THEN mov.movement_id ELSE NULL END) AS cant_pago_servicios,
		COUNT(DISTINCT CASE WHEN tt IN (20, 21) THEN mov.movement_id ELSE NULL END) AS cant_recargas_servicios,
		COUNT(DISTINCT CASE WHEN tt IN (29) THEN mov.movement_id ELSE NULL END) AS cant_recargas_mide,
		COUNT(DISTINCT CASE WHEN tt IN (2) THEN mov.movement_id ELSE NULL END) AS cant_cashin_cc,
		COUNT(DISTINCT CASE WHEN tt IN (3) THEN mov.movement_id ELSE NULL END) AS cant_transfer_to_cbu,
		COUNT(DISTINCT CASE WHEN tt IN (23) THEN mov.movement_id ELSE NULL END) AS cant_card_transfer_to_cbu,
		COUNT(DISTINCT CASE WHEN tt IN (5) THEN mov.movement_id ELSE NULL END) AS cant_t2t,
		COUNT(DISTINCT CASE WHEN tt IN (7) THEN mov.movement_id ELSE NULL END) AS cant_cashin_transfer,
		COUNT(DISTINCT CASE WHEN tt IN (14) THEN mov.movement_id ELSE NULL END) AS cant_cashin_dc,
		COUNT(DISTINCT CASE WHEN tt IN (25) THEN mov.movement_id ELSE NULL END) AS cant_qr_serv,
		COUNT(DISTINCT CASE WHEN tt IN (30) THEN mov.movement_id ELSE NULL END) AS cant_prepaid,
		COUNT(DISTINCT CASE WHEN tt IN (18) THEN mov.movement_id ELSE NULL END) AS cant_tx_worker,
		COUNT(DISTINCT CASE WHEN tt IS NULL THEN mov.movement_id ELSE NULL END) AS cant_tx_manuales,
		COUNT(DISTINCT CASE WHEN tt IS NULL and mov.origin_type = 'TAP' THEN mov.movement_id ELSE NULL END) AS cant_rendimiento,
		SUM(amount) AS sum_amount	  
	FROM accounts AS acc
	   LEFT JOIN bi.cx_user_movements AS mov ON acc.origin_id = mov.origin_id
	GROUP BY 1,2,3,4,5,6,7,8,9,10,11
) 