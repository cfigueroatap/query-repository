WITH a as (
SELECT o.external_ref, MIN(m.created_at) as primera_recarga_MIDE
FROM ledger.transaction t
LEFT JOIN ledger.movement m ON t.movement_id = m.id
LEFT JOIN ledger.origin  o ON t.origin_id = o.origin_id
WHERE t.transaction_type_id = 29 
GROUP BY o.external_ref
),

b as (
SELECT COUNT(external_ref) as cantidad_external, 
	SPLIT_PART(SPLIT_PART(metadata -> 'ticket' ->> 'Ticket','Medidor Nro. ',2),'"',1) AS medidor_mide
FROM ledger.movement m
LEFT JOIN ledger.origin o ON CAST(m.origin_id AS INTEGER) = o.origin_id
LEFT JOIN ledger.transaction AS t ON m.id=t.movement_id
WHERE t.transaction_type_id = 29
	AND date(m.created_at) <= '2021-12-02'
GROUP BY medidor_mide
HAVING COUNT(external_ref) <= 3
ORDER BY cantidad_external DESC
)

SELECT 
	m.created_at AS transaction_date, 
	t.transaction_id AS transaction, 
	ac.phone , 
	o.external_ref AS extref_origin, 
	'Bonificacion recargas MIDE' AS message,
	400 AS amount 
FROM ledger.movement m
LEFT JOIN ledger.transaction t 
	ON m.id = t.movement_id
LEFT JOIN ledger.origin o 
	ON t.origin_id = o.origin_id
LEFT JOIN app.account ac
	ON o.external_ref = ac.account_hash
LEFT JOIN a
	ON o.external_ref = a.external_ref
INNER JOIN b
	ON SPLIT_PART(SPLIT_PART(m.metadata -> 'ticket' ->> 'Ticket','Medidor Nro. ',2),'"',1) = b.medidor_mide
WHERE t.transaction_type_id = 29 
	AND a.primera_recarga_MIDE = m.created_at
	AND date(m.created_at) = '2021-12-02'
ORDER BY m.created_at
