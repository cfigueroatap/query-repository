WITH first AS (
SELECT 	pt.external_ref,
        min(m1.created_at) AS first_transaction,
		EXTRACT(MONTH FROM pt.created_at) AS mes
FROM prepaid.transaction pt
LEFT JOIN ledger.movement m1 ON m1.metadata ->> 'externalRequestId'::text = pt.external_request_id
WHERE
   		 pt.type = 'AUTH' AND
		 pt.status = 'COMPLETED' AND
		 m1.details = 'Pagaste a Rappi' AND
	 	 m1.amount >= -300
   GROUP BY mes, external_ref
   )

   
SELECT m.created_at AS transaction_date, 
	t.transaction_id AS transaction, 
	a.phone , 
	o.external_ref AS extref_origin, 
	'Bonificacion tarjeta prepaga' AS message,
	300 AS amount 
FROM ledger.movement m
LEFT JOIN ledger.transaction t 
	ON m.id = t.movement_id
LEFT JOIN ledger.origin o 
	ON t.origin_id = o.origin_id
LEFT JOIN app.account a 
	ON o.external_ref = a.account_hash
LEFT JOIN first f 
	ON o.external_ref = f.external_ref
WHERE t.transaction_type_id = 30 
	AND f.first_transaction = m.created_at
	AND date(m.created_at) BETWEEN '2021-11-10' AND '2021-11-11'
ORDER BY m.created_at