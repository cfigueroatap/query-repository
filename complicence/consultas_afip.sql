-- para personas uif (cuentas persona) 
select  ac.account_hash, ac.email, pe.cuit, pe.name, co.document, co.name , date(ac.created_at) as created_at,
pe.is_uif_person, pe.uif_approved, pe.is_exposed_person, co.is_exposed_person, pe.check_peype
from app.account as ac
left join app.person as pe on ac.account_id = pe.account_id
left join app.company as co on ac.account_id = co.account_id
where is_uif_person = 'true' and date(ac.created_at)>= '2021-09-24'


-- para cuentas con personas expuestas (en este caso empresas)

select  ac.account_hash, ac.email, co.document, co.name, date(ac.created_at) as created_at,
pe.uif_approved, co.is_exposed_person
from app.account as ac
left join app.person as pe on ac.account_id = pe.account_id
left join app.company as co on ac.account_id = co.account_id
where co.is_exposed_person = 'true' and date(ac.created_at)>= '2021-09-24'