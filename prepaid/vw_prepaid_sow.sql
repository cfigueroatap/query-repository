SELECT o1.name,
    o1.external_ref,
    to_char(m1.created_at, 'yyyy-mm'::text) AS "mes_año",
        CASE
            WHEN count(DISTINCT
            CASE
                WHEN t1.transaction_type_id = ANY (ARRAY[1, 22, 32, 33, 34]) THEN t1.transaction_id
                ELSE NULL::text
            END) > 0 THEN 1
            ELSE 0
        END AS pago_servicios,
        CASE
            WHEN count(DISTINCT
            CASE
                WHEN t1.transaction_type_id = ANY (ARRAY[20, 21]) THEN t1.transaction_id
                ELSE NULL::text
            END) > 0 THEN 1
            ELSE 0
        END AS pago_recargas,
        CASE
            WHEN count(DISTINCT
            CASE
                WHEN t1.transaction_type_id = 30 THEN t1.transaction_id
                ELSE NULL::text
            END) > 0 THEN 1
            ELSE 0
        END AS uso_prepaid
   FROM ledger.movement m1
     LEFT JOIN ledger.transaction t1 ON m1.id = t1.movement_id
     LEFT JOIN ledger.origin o1 ON t1.origin_id = o1.origin_id
  GROUP BY to_char(m1.created_at, 'yyyy-mm'), o1.external_ref, o1.name